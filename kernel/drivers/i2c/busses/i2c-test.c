
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/i2c.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/of_gpio.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/tlv.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/initval.h>
#include <linux/proc_fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/irq.h>


int audio_flag = 2;
EXPORT_SYMBOL(audio_flag);
static int audio_i2c_detect(struct i2c_client *i2c)
{
        struct i2c_msg msgs[2];
        char buf[2] = {0,0};
        int i = 0;
//es8323
        char reg = 0x28;

        msgs[0].flags = 0;
        msgs[0].addr  = 0x10;
        msgs[0].len   = 1;
        msgs[0].buf   = &reg;

        if(i2c_transfer(i2c->adapter, msgs, 1) >= 0){
                audio_flag = 0;
		return 0;
        }
//rt5631
        reg = 0x02;

        msgs[0].flags = 0;
        msgs[0].addr  = 0x1a;
        msgs[0].len   = 1;
        msgs[0].buf   = &reg;

        if(i2c_transfer(i2c->adapter, msgs, 1) >= 0){
                audio_flag = 1;
		return 0;
        }
	printk("xxxxxxxxxxxxxxxxxxxxxxxxx   audio_flag = %d\n",audio_flag);
        return 0;
}

static int audio_test_i2c_probe(struct i2c_client *i2c, const struct i2c_device_id *id)
{
	audio_i2c_detect(i2c);
	return 0;
}

static int audio_test_i2c_remove(struct i2c_client *client)
{
	return 0;
}

static const struct i2c_device_id audio_test_i2c_id[] = {
	{"audiotest", 0},
	{}
};

MODULE_DEVICE_TABLE(i2c, audio_test_i2c_id);

static const struct of_device_id audio_test_of_match[] = {
	{ .compatible = "audiotest", },
	{ }
};
MODULE_DEVICE_TABLE(of, audio_test_of_match);

static struct i2c_driver audio_test_i2c_driver = {
	.driver = {
		.name = "audio_test",
		.of_match_table = of_match_ptr(audio_test_of_match),
		},
	.probe = audio_test_i2c_probe,
	.remove = audio_test_i2c_remove,
	.id_table = audio_test_i2c_id,
};
module_i2c_driver(audio_test_i2c_driver);
