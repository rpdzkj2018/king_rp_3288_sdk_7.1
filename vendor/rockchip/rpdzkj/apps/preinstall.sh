#!/system/bin/sh
if [ ! -d "/system/app/preinstall" ];
then
	su
	chmod -R 777 /system/usr/preinstall
	for file in /system/usr/preinstall/*
	do
		/system/bin/pm install -r $file
		if [ $? -eq 0 ]; then
			rm -rf $file
		fi
	done
#	rm -rf /system/usr/preinstall
fi